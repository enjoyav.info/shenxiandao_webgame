package org.yunai.swjg.server.core.heartbeat;

import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.time.TimeService;

/**
 * 精确到毫秒的心跳计时器
 * User: yunai
 * Date: 13-5-24
 * Time: 上午10:03
 */
public class MilliHeartbeatTimer implements HeartbeatTimer{

    private static final TimeService timeService;
    static {
        timeService = BeanManager.getBean(TimeService.class);
    }

    /**
     * 周期, 单位：毫秒
     */
    private final long period;
    /**
     * 到时时间
     */
    private long timeUp;

    public MilliHeartbeatTimer(long period) {
        this.period = period;
        this.timeUp = timeService.now();
    }

    @Override
    public boolean timeUp() {
        return timeService.timeUp(timeUp);
    }

    @Override
    public void nextRound() {
        timeUp = timeService.now() + period;
    }
}
