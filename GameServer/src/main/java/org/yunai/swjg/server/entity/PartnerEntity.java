package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 伙伴对象
 * User: yunai
 * Date: 13-5-30
 * Time: 下午7:43
 */
public class PartnerEntity implements Entity<Integer> {

    /**
     * 伙伴编号
     */
    private int id;
    /**
     * 玩家编号
     */
    private int playerId;
    /**
     * 模版编号
     */
    private short templateId;
    /**
     * 等级
     */
    private short level;
    /**
     * 当前经验
     */
    private int exp;
    /**
     * 招募时间
     */
    private long addTime;
    /**
     * 解雇时间
     */
    private long removeTime;
    /**
     * 状态<br />
     * 1 - 队伍中<br />
     * 2 - 解雇
     */
    private byte status;
    // ==================== 培养的基础属性 ====================
    /**
     * 培养武力
     */
    private int developStrength;
    /**
     * 培养绝技
     */
    private int developStunt;
    /**
     * 培养法术
     */
    private int developMagic;
    // ==================== 丹药的基础属性 ====================
    /**
     * 丹药的武力
     */
    private String medicineStrength;
    /**
     * 丹药的绝技
     */
    private String medicineStunt;
    /**
     * 丹药的法术
     */
    private String medicineMagic;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public short getTemplateId() {
        return templateId;
    }

    public void setTemplateId(short templateId) {
        this.templateId = templateId;
    }

    public int getDevelopStrength() {
        return developStrength;
    }

    public void setDevelopStrength(int developStrength) {
        this.developStrength = developStrength;
    }

    public int getDevelopStunt() {
        return developStunt;
    }

    public void setDevelopStunt(int developStunt) {
        this.developStunt = developStunt;
    }

    public int getDevelopMagic() {
        return developMagic;
    }

    public void setDevelopMagic(int developMagic) {
        this.developMagic = developMagic;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public long getRemoveTime() {
        return removeTime;
    }

    public void setRemoveTime(long removeTime) {
        this.removeTime = removeTime;
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public String getMedicineStrength() {
        return medicineStrength;
    }

    public void setMedicineStrength(String medicineStrength) {
        this.medicineStrength = medicineStrength;
    }

    public String getMedicineStunt() {
        return medicineStunt;
    }

    public void setMedicineStunt(String medicineStunt) {
        this.medicineStunt = medicineStunt;
    }

    public String getMedicineMagic() {
        return medicineMagic;
    }

    public void setMedicineMagic(String medicineMagic) {
        this.medicineMagic = medicineMagic;
    }
}
