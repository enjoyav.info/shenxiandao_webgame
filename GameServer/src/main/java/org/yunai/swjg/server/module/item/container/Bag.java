package org.yunai.swjg.server.module.item.container;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 有容量上限的容器<br />
 * 容器中每一个元素占据一个index，每一个容器有一个id，并且属于一个Owner
 * User: yunai
 * Date: 13-4-6
 * Time: 上午10:35
 */
public interface Bag<C extends Containable, O> {

    /**
     * 背包在道具不存在的位置
     */
    public static final int BAG_INDEX_EMPTY = -1;

    /**
     * @return 容器类型
     */
    BagType getBagType();

    /**
     * @return 容器的容量上限
     */
    int getCapacity();

    /**
     * @return 所有者
     */
    O getOwner();

    /**
     * 通过索引获得其中的对象
     *
     * @param index 索引,范围[0, getCapacity()-1]
     * @return 如果此索引位置没有被占用，返回count为0的T的对象
     */
    C getByIndex(int index);

    /**
     * 触发加载后的事件
     */
    void onLoad();

    /**
     * 触发改便后的事件
     */
    void onChanged();

    /**
     * 背包类型枚举
     */
    public static enum BagType implements IndexedEnum {

        /**
         * 主道具包
         */
        PRIM((byte) 1),
        /**
         * 装备包
         */
        EQUIPMENT((byte) 2),
        /**
         * 仓库包
         */
        DEPOT((byte) 3),;

        private static final List<BagType> VALUES = Util.toIndexes(BagType.values());

        /**
         * 背包编号
         */
        private final byte type;

        private BagType(byte type) {
            this.type = type;
        }

        @Override
        public int getIndex() {
            return type;
        }

        public byte get() {
            return type;
        }

        /**
         * 根据背包编号获得枚举值
         *
         * @param bagId 背包编号
         * @return 背包枚举值
         */
        public static BagType valueOf(byte bagId) {
            return Util.valueOf(VALUES, bagId);
        }

        /**
         * 判断背包是否为空背包
         *
         * @param bagType 背包类型
         * @return 是否为空背包
         */
        public static boolean isNullBag(BagType bagType) {
            return bagType == null;
        }
    }
}