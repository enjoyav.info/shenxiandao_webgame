package org.yunai.swjg.server.module.currency;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_PlayerAddCurrencyResp;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

/**
 * 货币逻辑Service
 * User: yunai
 * Date: 13-5-12
 * Time: 上午1:16
 */
@Service
public class CurrencyService {

    /**
     * 验证玩家是否能够添加指定货币指定钱数<br />
     * 当添加钱小于等于0，认为验证通过<br />
     *
     * @param player    玩家信息
     * @param currency  货币枚举
     * @param incrNum   增加货币数
     * @param showError 是否发送错误消息给客户端
     * @return 是否能够添加钱
     */
    public boolean canGive(Player player, Currency currency, int incrNum, boolean showError) {
        if (incrNum <= 0) {
            return true;
        }
        if (incrNum > currency.getMax() || player.getCurrency(currency) >= currency.getMax() - incrNum) {
            if (showError) {
                switch (currency) {
                    case GOLD:
                        player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_CURRENCY_GOLD_OVER_MAX));
                        break;
                    case COIN:
                        player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_CURRENCY_COIN_OVER_MAX));
                        break;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * 添加钱，并提示客户端增加的钱数<br />
     * 该方法不进行是否添加钱的验证，所以，调用该方法前，请调用{@link #canGive(Player, Currency, int, boolean)}
     *
     * @param player   玩家信息
     * @param currency 货币枚举
     * @param incrNum  增加货币数
     */
    public void give(Player player, Currency currency, int incrNum) {
        if (incrNum <= 0) {
            return;
        }
        // 修改数据
        int newCount = player.getCurrency(currency) + incrNum;
        player.changeCurrency(currency, newCount);
        // 提示客户端
        player.message(new S_C_PlayerAddCurrencyResp(currency.getValue(), incrNum));
        // TODO 日志
    }

    /**
     * 验证玩家是否能够减少指定货币指定钱数
     *
     * @param player    玩家信息
     * @param currency  货币枚举
     * @param decrNum   减少货币数
     * @param showError 是否发送错误消息给客户端
     * @return 是否能够减少钱
     */
    public boolean canCost(Player player, Currency currency, int decrNum, boolean showError) {
        if (decrNum <= 0) {
            return true;
        }
        if (decrNum > player.getCurrency(currency)) {
            if (showError) {
                switch (currency) {
                    case GOLD:
                        player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_CURRENCY_GOLD_NOT_ENOUGH));
                        break;
                    case COIN:
                        player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_CURRENCY_COIN_NOT_ENOUGH));
                        break;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * 扣除钱，并提示客户端钱数的增量<br />
     * 该方法不进行是否扣除钱的验证，所以，调用该方法前，请调用{@link #canCost(Player, Currency, int, boolean)}
     *
     * @param player   玩家信息
     * @param currency 货币枚举
     * @param decrNum  减少货币数
     */
    public void cost(Player player, Currency currency, int decrNum) {
        if (decrNum <= 0) {
            return;
        }
        // 修改数据
        int newCount = player.getCurrency(currency) - decrNum;
        player.changeCurrency(currency, newCount);
        // 提示客户端
        player.message(new S_C_PlayerAddCurrencyResp(currency.getValue(), -decrNum));
        // TODO 日志
    }
}