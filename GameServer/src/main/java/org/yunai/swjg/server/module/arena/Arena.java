package org.yunai.swjg.server.module.arena;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.yunai.yfserver.common.HeartBeatable;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.common.Tickable;
import org.yunai.yfserver.message.MessageQueue;

/**
 * 竞技场
 * User: yunai
 * Date: 13-5-26
 * Time: 下午11:42
 */
public class Arena implements Tickable, HeartBeatable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.arena, Arena.class);

    /**
     * 消息队列
     */
    private MessageQueue msgQueue;
    /**
     * 数据更新器
     */
    private ArenaDataUpdater dataUpdater;

    public Arena() {
        this.msgQueue = new MessageQueue();
    }

    @Override
    public void tick() {
        while (!msgQueue.isEmpty()) {
            msgQueue.get().execute();
        }
    }

    @Override
    public void heartBeat() {
        updateData();
    }

    /**
     * 更新数据
     */
    private void updateData() {
        try {
            dataUpdater.process();
        } catch (Exception e) {
            LOGGER.error("[updateData] [exception:{}].", ExceptionUtils.getStackTrace(e));
        }
    }
}
