package org.yunai.protobuf.v1;

import java.util.*;

/**
 * 消息对象配置类
 * @author yunai
 * @version Jul 14, 2012
 */
public class Message {
	/**
	 * 类名. 即消息名称
	 */
	private String name;
	/**
	 * 属性列表. <K, V> 代表<属性次序, 属性>
	 */
	private SortedMap<Integer, Property> propertyMap;

	public String getName() {
		return name;
	}

	/**
	 * 
	 * @author yunai
	 * @version 2012-8-6
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public SortedMap<Integer, Property> getPropertyMap() {
		return propertyMap;
	}

	public void setPropertyMap(SortedMap<Integer, Property> propertyMap) {
		this.propertyMap = propertyMap;
	}

	/**
	 * @author yunai
	 * @
	 * @param index
	 * @param property
	 */
	public void addProperty(Integer index, Property property) {
		if (null == propertyMap) {
			propertyMap = new TreeMap<Integer, Property>();
		}
		propertyMap.put(index, property);
	}
	
	public static void main(String[] args) {
		List strList = new ArrayList();
		Set idSet = new HashSet();
	}
}
